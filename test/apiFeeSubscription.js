const { expect } = require('chai');
const APIFeeSubscription = artifacts.require('../contracts/APIFeeSubscription.sol');
const truffleConfig = require('../truffle.js');
const BigNumber = require('bignumber.js');

const Web3 = require('web3');
const web3 = new Web3(new Web3.providers.HttpProvider(`http://${truffleConfig.networks.test.host}:${truffleConfig.networks.test.port}`));

contract.only('APIFeeSubscription contract', async (accounts) => {
  const consumerAddress = accounts[0];
  const providerAddress = accounts[1];
  const apiFeeAddress   = accounts[2];

  let rawContract;
  let contractData;

  let consumerInitialBalance;
  let consumerBalanceAfterContractCreation;
  let providerInitialBalance;
  let apiFeeInitialBalance;

  const firstChargeConfig = {
    id: '1',
    termsHash: web3.utils.sha3('terms1'),
    credits: new BigNumber(100),
    consumedCredits: new BigNumber(0),
    valueInWei: new BigNumber('400')
  };

  const secondChargeConfig = {
    id: '2',
    termsHash: web3.utils.sha3('terms2'),
    credits: new BigNumber(200),
    consumedCredits: new BigNumber(0),
    valueInWei: new BigNumber('400')
  };

  const thirdChargeConfig = {
    id: '3',
    termsHash: web3.utils.sha3('terms3'),
    credits: new BigNumber(400),
    consumedCredits: new BigNumber(0),
    valueInWei: new BigNumber('400')
  };

  beforeEach(async () => {
    consumerInitialBalance = await getBalance(consumerAddress);
    providerInitialBalance = await getBalance(providerAddress);
    apiFeeInitialBalance = await getBalance(apiFeeAddress);

    rawContract = await APIFeeSubscription.new(
      providerAddress,
      apiFeeAddress,
      consumerAddress,
      firstChargeConfig.id,
      firstChargeConfig.termsHash,
      firstChargeConfig.credits.toNumber(),
      { from: consumerAddress, value: firstChargeConfig.valueInWei.toString(), gasPrice: 0 });

    const rawInitialCharge = await rawContract.getCharge(0, { from: consumerAddress });

    contractData = {
      address: rawContract.address,
      balance: await getBalance(rawContract.address),
      providerAddress: await rawContract.getProviderAddress({ from: consumerAddress }),
      consumerAddress: await rawContract.getConsumerAddress({ from: consumerAddress }),
      apiFeeAddress: await rawContract.getAPIFeeAddress({ from: consumerAddress }),
      signingAddress: await rawContract.getSigningAddress({ from: consumerAddress }),
      initialCharge: {
        id: rawInitialCharge[0],
        termsHash: rawInitialCharge[1],
        credits: rawInitialCharge[2],
        consumedCredits: rawInitialCharge[3],
        valueInWei: rawInitialCharge[4]
      }
    };

    consumerBalanceAfterContractCreation = await getBalance(consumerAddress);
  });

  it('should have spent consumer balance', async () =>
    expect(consumerInitialBalance.isEqualTo(consumerBalanceAfterContractCreation.plus(firstChargeConfig.valueInWei))).to.be.true);

  it('should have the correct balance', () => expect(contractData.balance.isEqualTo(firstChargeConfig.valueInWei)).to.be.true);

  it('should have the correct addresses', () => expect(contractData).contains({
    providerAddress: providerAddress,
    consumerAddress: consumerAddress,
    apiFeeAddress: apiFeeAddress,
    signingAddress: consumerAddress
  }));

  it('should have a single charge', async () => expect((await rawContract.getChargesLength({ from: consumerAddress })).toNumber()).to.eql(1));

  it('should have the correct initial charge id', () =>
    expect(contractData.initialCharge.id).to.eql(firstChargeConfig.id));

  it('should have the correct initial charge terms hash', () =>
    expect(contractData.initialCharge.termsHash).to.eql(firstChargeConfig.termsHash));

  it('should have the correct initial charge credits', () =>
    expect(firstChargeConfig.credits.isEqualTo(contractData.initialCharge.credits)).to.be.true);

  it('should have the correct initial charge consumed credits', () =>
    expect(firstChargeConfig.consumedCredits.isEqualTo(contractData.initialCharge.consumedCredits)).to.be.true);

  it('should have the correct initial charge value in Wei', () =>
    expect(firstChargeConfig.valueInWei.isEqualTo(contractData.initialCharge.valueInWei)).to.be.true);

  describe('when invalid address tries to charge credit', () => {
    let $err;

    beforeEach(async () => {
      const charge = {
        id: '3',
        termsHash: web3.utils.sha3('termsX'),
        credits: new BigNumber(100),
        valueInWei: new BigNumber('10000000000000')
      }

      try {
        await rawContract.chargeCredit(
          charge.id,
          charge.termsHash,
          charge.credits.toNumber(),
          { from: providerAddress, value: charge.valueInWei.toString(), gasPrice: 0 });
      } catch(err) {
        $err = err;
      }
    });

    it('should have failed when charging credit', () => expect($err).to.not.be.null);
    it('should have failed when charging credit and revert the operation', () => expect($err.message).to.contains('revert'));
  });

  describe('when consumer charges more credit', () => {
    let secondCharge;
    let chargesLength;
    let contractBalanceBeforeSecondCharge;
    let contractBalanceAfterSecondCharge;

    beforeEach(async () => {
      contractBalanceBeforeSecondCharge = await getBalance(contractData.address);

      await rawContract.chargeCredit(
        secondChargeConfig.id,
        secondChargeConfig.termsHash,
        secondChargeConfig.credits.toNumber(),
        { from: consumerAddress, value: secondChargeConfig.valueInWei.toString(), gasPrice: 0 });

      const rawSecondCharge = await rawContract.getCharge(1, { from: consumerAddress });

      secondCharge = {
        id: rawSecondCharge[0],
        termsHash: rawSecondCharge[1],
        credits: rawSecondCharge[2],
        consumedCredits: rawSecondCharge[3],
        valueInWei: rawSecondCharge[4]
      };

      contractBalanceAfterSecondCharge = await getBalance(contractData.address);
    });

    it('should have two charges', async () =>
      expect((await rawContract.getChargesLength({ from: consumerAddress })).toNumber()).to.eql(2));

    it('should have transfered weis to the contract', () =>
      expect(contractBalanceAfterSecondCharge.isEqualTo(contractBalanceBeforeSecondCharge.plus(secondChargeConfig.valueInWei))));

    it('should have the correct new charge id', () =>
      expect(secondCharge.id).to.eql(secondChargeConfig.id));

    it('should have the correct new charge terms hash', () =>
      expect(secondCharge.termsHash).to.eql(secondChargeConfig.termsHash));

    it('should have the correct new charge credits', () =>
      expect(secondChargeConfig.credits.isEqualTo(secondCharge.credits)).to.be.true);

    it('should have the correct new charge consumed credits', () =>
      expect(secondChargeConfig.consumedCredits.isEqualTo(secondCharge.consumedCredits)).to.be.true);

    it('should have the correct new charge value in Wei', () =>
      expect(secondChargeConfig.valueInWei.isEqualTo(secondCharge.valueInWei)).to.be.true);

    describe('and consumer charges more credit one more time', () => {
      let thirdCharge;
      let contractBalanceBeforeThirdCharge;
      let contractBalanceAfterThirdCharge;

      beforeEach(async () => {
        contractBalanceBeforeThirdCharge = await getBalance(contractData.address);

        await rawContract.chargeCredit(
          thirdChargeConfig.id,
          thirdChargeConfig.termsHash,
          thirdChargeConfig.credits.toNumber(),
          { from: consumerAddress, value: thirdChargeConfig.valueInWei.toString(), gasPrice: 0 });

        const rawThirdCharge = await rawContract.getCharge(2, { from: consumerAddress });

        thirdCharge = {
          id: rawThirdCharge[0],
          termsHash: rawThirdCharge[1],
          credits: rawThirdCharge[2],
          consumedCredits: rawThirdCharge[3],
          valueInWei: rawThirdCharge[4]
        };

        contractBalanceAfterThirdCharge = await getBalance(contractData.address);
      });

      it('should have three charges', async () =>
        expect((await rawContract.getChargesLength({ from: consumerAddress })).toNumber()).to.eql(3));

      it('should have transfered weis to the contract', () =>
        expect(contractBalanceAfterThirdCharge.isEqualTo(contractBalanceBeforeThirdCharge.plus(thirdChargeConfig.valueInWei))));

      it('should have the correct new charge id', () =>
        expect(thirdCharge.id).to.eql(thirdChargeConfig.id));

      it('should have the correct new charge terms hash', () =>
        expect(thirdCharge.termsHash).to.eql(thirdChargeConfig.termsHash));

      it('should have the correct new charge credits', () =>
        expect(thirdChargeConfig.credits.isEqualTo(thirdCharge.credits)).to.be.true);

      it('should have the correct new charge consumed credits', () =>
        expect(thirdChargeConfig.consumedCredits.isEqualTo(thirdCharge.consumedCredits)).to.be.true);

      it('should have the correct new charge value in Wei', () =>
        expect(thirdChargeConfig.valueInWei.isEqualTo(thirdCharge.valueInWei)).to.be.true);

      describe('and provider withdraw', () => {
        let consumerBalanceAfterWithdraw;
        let providerBalanceBeforeWithdraw;
        let providerBalanceAfterWithdraw;
        let apiFeeBalanceAfterWithdraw;
        let contractBalanceBeforeWithdraw;
        let contractBalanceAfterWithdraw;
        let weisToBeTransfered;

        describe('with invalid signature', () => {
          let $err;

          beforeEach(async () => {
            const credits = 1;
            const signature = await signServerSide(consumerAddress, 'invalid-data-in-the-signature', credits);

            try {
              await rawContract.withdraw(credits, signature.v, signature.r, signature.s, { from: providerAddress, gasPrice: 0 });
            } catch (err) {
              $err = err;
            }
          });

          it('should have failed', () => expect($err).to.not.be.null);
        });

        describe('with the first charge partially', () => {
          beforeEach(async () => {
            contractBalanceBeforeWithdraw = await getBalance(contractData.address);
            providerBalanceBeforeWithdraw = await getBalance(providerAddress);

            const credits = 50;
            const signature = await signServerSide(consumerAddress, contractData.address, credits);

            const initialChargeCreditPrice = firstChargeConfig.valueInWei.dividedBy(firstChargeConfig.credits);
            weisToBeTransfered = initialChargeCreditPrice * credits;

            await rawContract.withdraw(credits, signature.v, signature.r, signature.s, { from: providerAddress, gasPrice: 0 });

            consumerBalanceAfterWithdraw = await getBalance(consumerAddress);
            providerBalanceAfterWithdraw = await getBalance(providerAddress);
            apiFeeBalanceAfterWithdraw = await getBalance(apiFeeAddress);
            contractBalanceAfterWithdraw = await getBalance(contractData.address);
          });

          it('should have transfered weis from the contract', () =>
            expect(contractBalanceBeforeWithdraw.isEqualTo(contractBalanceAfterWithdraw.plus(weisToBeTransfered))).to.be.true);

          it('should have transfered weis to the provider', () =>
            expect(providerBalanceAfterWithdraw.isEqualTo(providerBalanceBeforeWithdraw.plus(weisToBeTransfered))).to.be.true);

          it('should have consumed credits from the first charge', async () =>
            expect((await rawContract.getCharge(0, { from: consumerAddress }))[3].toNumber()).to.eql(50));

          it('should have not consumed credits from the second charge', async () =>
            expect((await rawContract.getCharge(1, { from: consumerAddress }))[3].toNumber()).to.eql(0));

          it('should have not consumed credits from the third charge', async () =>
            expect((await rawContract.getCharge(2, { from: consumerAddress }))[3].toNumber()).to.eql(0));

          describe('and withdraw again with old signature', () => {
            beforeEach(async () => {
              contractBalanceBeforeWithdraw = await getBalance(contractData.address);

              const credits = 25;
              const signature = await signServerSide(consumerAddress, contractData.address, credits);

              await rawContract.withdraw(credits, signature.v, signature.r, signature.s, { from: providerAddress, gasPrice: 0 });

              contractBalanceAfterWithdraw = await getBalance(contractData.address);
            });

            it('should have the same balance', () =>
              expect(contractBalanceBeforeWithdraw.isEqualTo(contractBalanceAfterWithdraw)).to.be.true);
          });

          describe('and withdraw another portion of the first charge', () => {
            beforeEach(async () => {
              contractBalanceBeforeWithdraw = await getBalance(contractData.address);
              providerBalanceBeforeWithdraw = await getBalance(providerAddress);

              const credits = 75;
              const signature = await signServerSide(consumerAddress, contractData.address, credits);

              const initialChargeCreditPrice = firstChargeConfig.valueInWei.dividedBy(firstChargeConfig.credits);
              weisToBeTransfered = initialChargeCreditPrice.multipliedBy(25);

              await rawContract.withdraw(credits, signature.v, signature.r, signature.s, { from: providerAddress, gasPrice: 0 });

              contractBalanceAfterWithdraw = await getBalance(contractData.address);
              providerBalanceAfterWithdraw = await getBalance(providerAddress);
            });

            it('should have transfered weis from the contract', () =>
              expect(contractBalanceBeforeWithdraw.isEqualTo(contractBalanceAfterWithdraw.plus(weisToBeTransfered))).to.be.true);

            it('should have transfered weis to the provider', () =>
              expect(providerBalanceAfterWithdraw.isEqualTo(providerBalanceBeforeWithdraw.plus(weisToBeTransfered))).to.be.true);

            it('should have consumed credits from the first charge', async () =>
              expect((await rawContract.getCharge(0, { from: consumerAddress }))[3].toNumber()).to.eql(75));

            it('should have not consumed credits from the second charge', async () =>
              expect((await rawContract.getCharge(1, { from: consumerAddress }))[3].toNumber()).to.eql(0));

            it('should have not consumed credits from the third charge', async () =>
              expect((await rawContract.getCharge(2, { from: consumerAddress }))[3].toNumber()).to.eql(0));
          });

          describe('and withdraw the rest of the first charge and a portion of the second', () => {
            beforeEach(async () => {
              contractBalanceBeforeWithdraw = await getBalance(contractData.address);
              providerBalanceBeforeWithdraw = await getBalance(providerAddress);

              const credits = 250;
              const signature = await signServerSide(consumerAddress, contractData.address, credits);

              const firstChargeCreditPrice = firstChargeConfig.valueInWei.dividedBy(firstChargeConfig.credits);
              const secondChargeCreditPrice = secondChargeConfig.valueInWei.dividedBy(secondChargeConfig.credits);

              const initialChargePortionInWeis = firstChargeCreditPrice.multipliedBy(50);
              const secondChargePortionInWeis = secondChargeCreditPrice.multipliedBy(150);

              weisToBeTransfered = initialChargePortionInWeis.plus(secondChargePortionInWeis);

              await rawContract.withdraw(credits, signature.v, signature.r, signature.s, { from: providerAddress, gasPrice: 0 });

              contractBalanceAfterWithdraw = await getBalance(contractData.address);
              providerBalanceAfterWithdraw = await getBalance(providerAddress);
            });

            it('should have transfered weis from the contract', () =>
              expect(contractBalanceBeforeWithdraw.isEqualTo(contractBalanceAfterWithdraw.plus(weisToBeTransfered))).to.be.true);

            it('should have transfered weis to the provider', () =>
              expect(providerBalanceAfterWithdraw.isEqualTo(providerBalanceBeforeWithdraw.plus(weisToBeTransfered))).to.be.true);

            it('should have consumed credits from the first charge', async () =>
              expect((await rawContract.getCharge(0, { from: consumerAddress }))[3].toNumber()).to.eql(100));

            it('should have not consumed credits from the second charge', async () =>
              expect((await rawContract.getCharge(1, { from: consumerAddress }))[3].toNumber()).to.eql(150));

            it('should have not consumed credits from the third charge', async () =>
              expect((await rawContract.getCharge(2, { from: consumerAddress }))[3].toNumber()).to.eql(0));
          });
        });

        describe('with the first charge full and the second partially', () => {
          beforeEach(async () => {
            contractBalanceBeforeWithdraw = await getBalance(contractData.address);
            providerBalanceBeforeWithdraw = await getBalance(providerAddress);

            const credits = 250;
            const signature = await signServerSide(consumerAddress, contractData.address, credits);

            const secondChargeCreditPrice = secondChargeConfig.valueInWei.dividedBy(secondChargeConfig.credits);

            const initialChargePortionInWeis = firstChargeConfig.valueInWei;
            const secondChargePortionInWeis = secondChargeCreditPrice.multipliedBy(150);

            weisToBeTransfered = initialChargePortionInWeis.plus(secondChargePortionInWeis);

            await rawContract.withdraw(credits, signature.v, signature.r, signature.s, { from: providerAddress, gasPrice: 0 });

            consumerBalanceAfterWithdraw = await getBalance(consumerAddress);
            providerBalanceAfterWithdraw = await getBalance(providerAddress);
            apiFeeBalanceAfterWithdraw = await getBalance(apiFeeAddress);
            contractBalanceAfterWithdraw = await getBalance(contractData.address);
          });

          it('should have transfered weis from the contract', () =>
            expect(contractBalanceBeforeWithdraw.isEqualTo(contractBalanceAfterWithdraw.plus(weisToBeTransfered))).to.be.true);

          it('should have transfered weis to the provider', () =>
            expect(providerBalanceAfterWithdraw.isEqualTo(providerBalanceBeforeWithdraw.plus(weisToBeTransfered))).to.be.true);

          it('should have consumed credits from the first charge', async () =>
            expect((await rawContract.getCharge(0, { from: consumerAddress }))[3].toNumber()).to.eql(100));

          it('should have consumed credits from the second charge', async () =>
            expect((await rawContract.getCharge(1, { from: consumerAddress }))[3].toNumber()).to.eql(150));

          it('should have not consumed credits from the third charge', async () =>
            expect((await rawContract.getCharge(2, { from: consumerAddress }))[3].toNumber()).to.eql(0));
        });

        describe('with the first and second charge full and the third partially', () => {
          beforeEach(async () => {
            contractBalanceBeforeWithdraw = await getBalance(contractData.address);
            providerBalanceBeforeWithdraw = await getBalance(providerAddress);

            const credits = 500;
            const signature = await signServerSide(consumerAddress, contractData.address, credits);

            const thirdChargeCreditPrice = thirdChargeConfig.valueInWei.dividedBy(thirdChargeConfig.credits);

            const initialChargePortionInWeis = firstChargeConfig.valueInWei;
            const secondChargePortionInWeis = secondChargeConfig.valueInWei;
            const thirdChargePortionInWeis = thirdChargeCreditPrice.multipliedBy(200);

            weisToBeTransfered = initialChargePortionInWeis.plus(secondChargePortionInWeis).plus(thirdChargePortionInWeis);

            await rawContract.withdraw(credits, signature.v, signature.r, signature.s, { from: providerAddress, gasPrice: 0 });

            consumerBalanceAfterWithdraw = await getBalance(consumerAddress);
            providerBalanceAfterWithdraw = await getBalance(providerAddress);
            apiFeeBalanceAfterWithdraw = await getBalance(apiFeeAddress);
            contractBalanceAfterWithdraw = await getBalance(contractData.address);
          });

          it('should have transfered weis from the contract', () =>
            expect(contractBalanceBeforeWithdraw.isEqualTo(contractBalanceAfterWithdraw.plus(weisToBeTransfered))).to.be.true);

          it('should have transfered weis to the provider', () =>
            expect(providerBalanceAfterWithdraw.isEqualTo(providerBalanceBeforeWithdraw.plus(weisToBeTransfered))).to.be.true);

          it('should have consumed credits from the first charge', async () =>
            expect((await rawContract.getCharge(0, { from: consumerAddress }))[3].toNumber()).to.eql(100));

          it('should have consumed credits from the second charge', async () =>
            expect((await rawContract.getCharge(1, { from: consumerAddress }))[3].toNumber()).to.eql(200));

          it('should have not consumed credits from the third charge', async () =>
            expect((await rawContract.getCharge(2, { from: consumerAddress }))[3].toNumber()).to.eql(200));
        });

        describe('with the first, second and third charge exactly full', () => {
          beforeEach(async () => {
            contractBalanceBeforeWithdraw = await getBalance(contractData.address);
            providerBalanceBeforeWithdraw = await getBalance(providerAddress);

            const credits = 700;
            const signature = await signServerSide(consumerAddress, contractData.address, credits);

            const initialChargePortionInWeis = firstChargeConfig.valueInWei;
            const secondChargePortionInWeis = secondChargeConfig.valueInWei;
            const thirdChargePortionInWeis = thirdChargeConfig.valueInWei;

            weisToBeTransfered = initialChargePortionInWeis.plus(secondChargePortionInWeis).plus(thirdChargePortionInWeis);

            await rawContract.withdraw(credits, signature.v, signature.r, signature.s, { from: providerAddress, gasPrice: 0 });

            consumerBalanceAfterWithdraw = await getBalance(consumerAddress);
            providerBalanceAfterWithdraw = await getBalance(providerAddress);
            apiFeeBalanceAfterWithdraw = await getBalance(apiFeeAddress);
            contractBalanceAfterWithdraw = await getBalance(contractData.address);
          });

          it('should have no more weis left', () =>
            expect(contractBalanceAfterWithdraw.isZero()).to.be.true);

          it('should have transfered weis from the contract', () =>
            expect(contractBalanceBeforeWithdraw.isEqualTo(contractBalanceAfterWithdraw.plus(weisToBeTransfered))).to.be.true);

          it('should have transfered weis to the provider', () =>
            expect(providerBalanceAfterWithdraw.isEqualTo(providerBalanceBeforeWithdraw.plus(weisToBeTransfered))).to.be.true);

          it('should have consumed credits from the first charge', async () =>
            expect((await rawContract.getCharge(0, { from: consumerAddress }))[3].toNumber()).to.eql(100));

          it('should have consumed credits from the second charge', async () =>
            expect((await rawContract.getCharge(1, { from: consumerAddress }))[3].toNumber()).to.eql(200));

          it('should have not consumed credits from the third charge', async () =>
            expect((await rawContract.getCharge(2, { from: consumerAddress }))[3].toNumber()).to.eql(400));

          describe('and withdraw again with a greater amout of credits signed', () => {
            beforeEach(async () => {
              contractBalanceBeforeWithdraw = await getBalance(contractData.address);

              const credits = 1000;
              const signature = await signServerSide(consumerAddress, contractData.address, credits);

              await rawContract.withdraw(credits, signature.v, signature.r, signature.s, { from: providerAddress, gasPrice: 0 });

              contractBalanceAfterWithdraw = await getBalance(contractData.address);
            });

            it('should have the same balance', () =>
              expect(contractBalanceBeforeWithdraw.isEqualTo(contractBalanceAfterWithdraw)).to.be.true);
          });
        });

        describe('with the first, second and third charge full and signed credits greater than max credits', () => {
          beforeEach(async () => {
            contractBalanceBeforeWithdraw = await getBalance(contractData.address);
            providerBalanceBeforeWithdraw = await getBalance(providerAddress);

            const credits = 1000;
            const signature = await signServerSide(consumerAddress, contractData.address, credits);

            const initialChargePortionInWeis = firstChargeConfig.valueInWei;
            const secondChargePortionInWeis = secondChargeConfig.valueInWei;
            const thirdChargePortionInWeis = thirdChargeConfig.valueInWei;

            weisToBeTransfered = initialChargePortionInWeis.plus(secondChargePortionInWeis).plus(thirdChargePortionInWeis);

            await rawContract.withdraw(credits, signature.v, signature.r, signature.s, { from: providerAddress, gasPrice: 0 });

            consumerBalanceAfterWithdraw = await getBalance(consumerAddress);
            providerBalanceAfterWithdraw = await getBalance(providerAddress);
            apiFeeBalanceAfterWithdraw = await getBalance(apiFeeAddress);
            contractBalanceAfterWithdraw = await getBalance(contractData.address);
          });

          it('should have no more weis left', () =>
            expect(contractBalanceAfterWithdraw.isZero()).to.be.true);

          it('should have transfered weis from the contract', () =>
            expect(contractBalanceBeforeWithdraw.isEqualTo(contractBalanceAfterWithdraw.plus(weisToBeTransfered))).to.be.true);

          it('should have transfered weis to the provider', () =>
            expect(providerBalanceAfterWithdraw.isEqualTo(providerBalanceBeforeWithdraw.plus(weisToBeTransfered))).to.be.true);

          it('should have consumed credits from the first charge', async () =>
            expect((await rawContract.getCharge(0, { from: consumerAddress }))[3].toNumber()).to.eql(100));

          it('should have consumed credits from the second charge', async () =>
            expect((await rawContract.getCharge(1, { from: consumerAddress }))[3].toNumber()).to.eql(200));

          it('should have not consumed credits from the third charge', async () =>
            expect((await rawContract.getCharge(2, { from: consumerAddress }))[3].toNumber()).to.eql(400));
        });
      });
    });
  });

  describe('verify signature', () => {
    let $err;

    describe('and signature is valid', () => {
      beforeEach(async () => {
        const credits = 1;
        const signature = await signServerSide(consumerAddress, contractData.address, credits);

        try {
          await rawContract.verifySignature(credits, signature.v, signature.r, signature.s, { from: providerAddress, gasPrice: 0 });
        } catch (err) {
          $err = err;
        }
      });

      it('should have not failed', () => expect($err).to.be.undefined);
    });

    describe('and signature is invalid', () => {
      beforeEach(async () => {
        const credits = 1;
        const signature = await signServerSide(consumerAddress, 'invalid-data-in-the-signature', credits);

        try {
          await rawContract.verifySignature(credits, signature.v, signature.r, signature.s, { from: providerAddress, gasPrice: 0 });
        } catch (err) {
          $err = err;
        }
      });

      it('should have failed', () => expect($err).to.not.be.null);
    });
  });

  // TODO invalid address tries to withdraw
  // TODO invalid signature tries to withdraw

  // Helper functions
  async function signServerSide(address, ...message) {
    let messageHash = web3.utils.soliditySha3(...message);
    const signature = await web3.eth.sign(messageHash, address);

    return splitSignature(signature);
  }

  function splitSignature(signature) {
    const v = web3.utils.hexToNumber('0x' + signature.slice(130, 132));

    return {
      r: signature.slice(0, 66),
      s: '0x' + signature.slice(66, 130),
      v:  v >= 27 ? v : v + 27
    };
  }

  async function getBalance(address) {
    return new BigNumber(await web3.eth.getBalance(address));
  }
});
