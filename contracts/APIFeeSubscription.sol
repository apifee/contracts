pragma solidity ^0.4.24;

import 'openzeppelin-solidity/contracts/math/SafeMath.sol';

contract APIFeeSubscription {

  struct Charge {
    string id;
    bytes32 termsHash;
    uint credits;
    uint consumedCredits;
    uint valueInWei;
    // TODO fee
  }

  address providerAddress;
  address consumerAddress;
  address apiFeeAddress;
  address signingAddress;
  Charge[] charges;

  constructor(
    address _providerAddress,
    address _apiFeeAddress,
    address _signingAddress,
    string _initialChargeId,
    bytes32 _initialChargeTermsHash,
    uint _initialChargeCredits
  ) payable public {

    providerAddress = _providerAddress;
    consumerAddress = msg.sender;
    apiFeeAddress = _apiFeeAddress;
    signingAddress = _signingAddress;
    // TODO Check how to reuse
    charges.push(Charge({ id: _initialChargeId, termsHash: _initialChargeTermsHash, credits: _initialChargeCredits, consumedCredits: 0, valueInWei: msg.value }));
  }

  function getBalance() external view returns (uint) {
    return address(this).balance;
  }

  function getProviderAddress() external view returns (address) {
    return providerAddress;
  }

  function getConsumerAddress() external view returns (address) {
    return consumerAddress;
  }

  function getAPIFeeAddress() external view returns (address) {
    return apiFeeAddress;
  }

  function getSigningAddress() external view returns (address) {
    return signingAddress;
  }

  // TODO Refactor to serialize the Charge and return an array of bytes
  function getCharge(uint8 index) external view returns (string, bytes32, uint, uint, uint) {
    Charge storage currentCharge = charges[index];
    return (currentCharge.id, currentCharge.termsHash, currentCharge.credits, currentCharge.consumedCredits, currentCharge.valueInWei);
  }

  function getChargesLength() external view returns (uint) {
    return charges.length;
  }

  function chargeCredit(string _id, bytes32 _termsHash, uint _credits) onlyConsumer payable public {
    // TODO Check valueInWei / credits has no decimals
    charges.push(Charge({ id: _id, termsHash: _termsHash, credits: _credits, consumedCredits: 0, valueInWei: msg.value }));
  }

  function verifySignature(uint signedCredits, uint8 v, bytes32 r, bytes32 s) view public {
    // Verify the signingAddress has signed the promise
    bytes32 messageHash = keccak256(abi.encodePacked(address(this), signedCredits));
    bytes memory prefix = "\x19Ethereum Signed Message:\n32";
    messageHash = keccak256(abi.encodePacked(prefix, messageHash));
    address signer = ecrecover(messageHash, v, r, s);
    require(signer == signingAddress);
  }

  function withdraw(uint signedCredits, uint8 v, bytes32 r, bytes32 s) external onlyProvider {
    verifySignature(signedCredits, v, r, s);

    // Compute and transfer the corresponding amount of Weis to the provider
    uint creditsAcum = 0;
    uint valueToBeTransferedInWei = 0;
    for (uint chargeIndex = 0; chargeIndex < charges.length; chargeIndex = SafeMath.add(chargeIndex, 1)) {
      Charge storage currentCharge = charges[chargeIndex];

      // Check if it's using an "old" signature
      if (signedCredits <= SafeMath.add(creditsAcum, currentCharge.consumedCredits)) {
        break;
      }

      // No more credits left
      if (currentCharge.credits <= currentCharge.consumedCredits) {
        creditsAcum = SafeMath.add(creditsAcum, currentCharge.credits);
        continue;
      }

      uint creditsToBeTransfered = 0;

      // Check full transfer
      bool fullTransfer = SafeMath.add(creditsAcum, currentCharge.credits) < signedCredits;

      // Compute the credits to be transfered
      if (fullTransfer) {
        creditsToBeTransfered = currentCharge.credits - currentCharge.consumedCredits;
      } else {
        // Partial transfer
        creditsToBeTransfered = signedCredits - creditsAcum - currentCharge.consumedCredits;
      }

      // Compute amount of wei to be transfered
      uint creditPriceInWei = SafeMath.div(currentCharge.valueInWei, currentCharge.credits);

      // Add current value to be transfered in wei to acum value to be transfered
      valueToBeTransferedInWei = SafeMath.add(valueToBeTransferedInWei, SafeMath.mul(creditsToBeTransfered, creditPriceInWei));

      // Update consumedCredits
      currentCharge.consumedCredits = SafeMath.add(currentCharge.consumedCredits, creditsToBeTransfered);

      if (!fullTransfer) {
        break;
      }

      creditsAcum = SafeMath.add(creditsAcum, currentCharge.credits);
    }

    // transfer weis to the provider
    if (valueToBeTransferedInWei > 0) {
      providerAddress.transfer(valueToBeTransferedInWei);
    }
  }

  // TODO Add cancel charge

  modifier onlyConsumer() {
    require(msg.sender == consumerAddress);
    _;
  }

  modifier onlyProvider() {
    require(msg.sender == providerAddress);
    _;
  }
}
