const APIFeeSubscription = artifacts.require("./APIFeeSubscription.sol");
const BigNumber = require('bignumber.js');
const utils = require('../src/utils.js')();
const truffleConfig = require('../truffle.js');
const Web3 = require('web3');
const web3 = new Web3(new Web3.providers.HttpProvider(`http://${truffleConfig.networks.test.host}:${truffleConfig.networks.test.port}`));

module.exports = async function(deployer, network, accounts) {
  const consumerAddress = accounts[0];
  const providerAddress = accounts[1];
  const apiFeeAddress   = accounts[2];

  const firstChargeConfig = {
    id: '1',
    termsHash: web3.utils.sha3('terms1'),
    credits: new BigNumber(10),
    consumedCredits: new BigNumber(0),
    valueInWei: new BigNumber('40')
  };

  await deployer.deploy(APIFeeSubscription,
    providerAddress,
    apiFeeAddress,
    consumerAddress,
    firstChargeConfig.id,
    firstChargeConfig.termsHash,
    firstChargeConfig.credits.toNumber(),
    { from: consumerAddress, value: firstChargeConfig.valueInWei.toString(), gasPrice: 0 }
  );

  const contract = await APIFeeSubscription.deployed();

  // Second charge
  const secondChargeConfig = {
    id: '2',
    termsHash: web3.utils.sha3('terms1'),
    credits: new BigNumber(5),
    consumedCredits: new BigNumber(0),
    valueInWei: new BigNumber('10')
  };

  await contract.chargeCredit(
    secondChargeConfig.id,
    secondChargeConfig.termsHash,
    secondChargeConfig.credits.toNumber(),
    { from: consumerAddress, value: secondChargeConfig.valueInWei.toString(), gasPrice: 0 });

  // Sign samples
  const signAddress = accounts[0];

  console.log('Generating signatures...');

  for (let i = 1; i < 23; i++) {
    const signatureParts = await utils.signServerSide(web3, signAddress, contract.address, i);
    console.log(`${i}: ${signatureParts.full}`);
    console.log('Signature parts', signatureParts);
    console.log('Validating signature...');

    try {
      await contract.verifySignature(i, signatureParts.v, signatureParts.r, signatureParts.s);
      console.log('Valid signature');
    } catch(err) {
      console.log('Invalid signature', err);
    }
  }

  console.log('Finishing signatures...');
};
