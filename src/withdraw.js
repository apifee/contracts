const BigNumber = require('bignumber.js');
const APIFeeSubscription = require("../build/contracts/APIFeeSubscription.json");
const truffleConfig = require('../truffle.js');
const Web3 = require('web3');
const web3 = new Web3(new Web3.providers.HttpProvider(`http://${truffleConfig.networks.test.host}:${truffleConfig.networks.test.port}`));

// Accounts
(async () => {
  const contractAddress = '0xc91c95f67622a51040ddd1c4bf47b8aaab374e86';
  const consumerAddress = '0x381b622a1910296c5084cf93cd1ad19d5719c905';
  const providerAddress = '0x9c3493f110093e01dc2081cadf5845489f6e910e';

  const r7 = '0x824690ce5ee2be4d99e937c3a7ba06d5d61599a874132f4d4341b4128ee058cf';
  const s7 = '0x635404a1d2cf54648b1cfeb8ac481eb84020e4c05ed9e3cc94dda1200e2ec2db';
  const v7 = 27;

  const contract = new web3.eth.Contract(APIFeeSubscription.abi, contractAddress);

  await contract.methods.withdraw(7, v7, r7, s7).send({ from: providerAddress, gas: 500000, gasPrice: 0 });
})();
