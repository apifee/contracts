const BigNumber = require('bignumber.js');
const APIFeeSubscription = require("../build/contracts/APIFeeSubscription.json");
const truffleConfig = require('../truffle.js');
const Web3 = require('web3');
const web3 = new Web3(new Web3.providers.HttpProvider(`http://${truffleConfig.networks.test.host}:${truffleConfig.networks.test.port}`));

// Accounts
(async () => {
  const contractAddress = '0xc91c95f67622a51040ddd1c4bf47b8aaab374e86';
  const contract = new web3.eth.Contract(APIFeeSubscription.abi, contractAddress);

  async function getBalance(address) {
    return new BigNumber(await web3.eth.getBalance(address));
  }

  async function getCharges(web3Contract) {
    const charges = [];
    const chargesLength = await web3Contract.methods.getChargesLength().call();

    let accumCredits = 0;
    for (let i = 0; i < chargesLength; i++) {
      const rawCharge = await web3Contract.methods.getCharge(i).call();

      const charge = {
        accumCredits: accumCredits,
        id: rawCharge[0],
        termsHash: rawCharge[1],
        credits: parseInt(rawCharge[2]),
        consumedCredits: parseInt(rawCharge[3]),
        valueInWei: rawCharge[4]
      };

      charges.push(charge);
      accumCredits += charge.credits;
    }

    return charges;
  }

  const balance = (await getBalance(contractAddress)).toString();
  const providerAddress = await contract.methods.getProviderAddress().call();
  const consumerAddress = await contract.methods.getConsumerAddress().call();
  const apiFeeAddress = await contract.methods.getAPIFeeAddress().call();
  const signingAddress = await contract.methods.getSigningAddress().call();
  const charges = await getCharges(contract);

  console.log('contract', {
    balance,
    providerAddress,
    consumerAddress,
    apiFeeAddress,
    signingAddress,
    charges
  })
})();
