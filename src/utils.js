module.exports = function utils() {
  return {
    signServerSide,
    signClientSide
  };

  async function signServerSide(web3, address, ...message) {
    const messageHash = web3.utils.soliditySha3(...message);
    const signature = await web3.eth.sign(messageHash, address);

    return splitSignature(web3, signature);
  }

  async function signClientSide(web3, privateKey, ...message) {
    // eslint-disable-next-line global-require
    const Account = require('eth-lib/lib/account');
    const messageHash = web3.utils.soliditySha3(...message);
    const ethMessage = web3.utils.toHex('\x19Ethereum Signed Message:\n32') + messageHash.substring(2);
    const hash = web3.utils.sha3(ethMessage);
    const signature = Account.sign(hash, privateKey);

    return splitSignature(web3, signature);
  }

  function splitSignature(web3, signature) {
    const v = web3.utils.hexToNumber(`0x${signature.slice(130, 132)}`);

    return {
      r: signature.slice(0, 66),
      s: `0x${signature.slice(66, 130)}`,
      v:  v >= 27 ? v : v + 27,
      full: signature
    };
  }
};
